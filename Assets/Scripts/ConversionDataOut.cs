using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using AppsFlyerSDK;

public class ConversionDataOut : MonoBehaviour, IAppsFlyerConversionData
{
    [SerializeField] private Text conversionText;
    [SerializeField] private Button startButton;
    void Start()
    {
        /* AppsFlyer.setDebugLog(true); */
        AppsFlyer.initSDK("ZLigGqGzDdxGMT7QBPjsMG", null, this);
        AppsFlyer.startSDK();

        AppsFlyer.getConversionData(conversionText.text);
    }
    public void onConversionDataSuccess(string conversionData)
    {
        AppsFlyer.AFLog("onConversionDataSuccess", conversionData);
        Dictionary<string, object> conversionDataDictionary = AppsFlyer.CallbackStringToDictionary(conversionData);
        foreach (var item in conversionDataDictionary.Keys)
        {
            foreach(var inner in conversionDataDictionary.Values)
            {
                conversionText.text += $"{item}:{inner}";
            }
        }
        ShowButtonToStart();

        // add deferred deeplink logic here
    }

    public void ShowButtonToStart()
    {
        startButton.gameObject.SetActive(true);
        startButton.onClick.AddListener(() => SceneManager.LoadScene(1));
    }

    public void onConversionDataFail(string error)
    {
        AppsFlyer.AFLog("onConversionDataFail", error);
        conversionText.text = error;
    }

    public void onAppOpenAttribution(string attributionData)
    {
        AppsFlyer.AFLog("onAppOpenAttribution", attributionData);
        Dictionary<string, object> attributionDataDictionary = AppsFlyer.CallbackStringToDictionary(attributionData);
        // add direct deeplink logic here
    }

    public void onAppOpenAttributionFailure(string error)
    {
        AppsFlyer.AFLog("onAppOpenAttributionFailure", error);
    }
}
