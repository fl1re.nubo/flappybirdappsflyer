using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float velocity = 3f;
    public Rigidbody2D rb;
    [SerializeField]private AudioController audioController;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if(Input.GetMouseButtonDown(0) && Time.timeScale != 0)
        {
            rb.velocity = Vector2.up * velocity;
            audioController.PlaySound(AudioController.Sounds.wing);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Obstacle")
        {
            EventManager.GameOver();
            audioController.PlaySound(AudioController.Sounds.hit);
        }
        if(collision.tag == "ObstacleHolder")
        {
            EventManager.OnGameScore();
            audioController.PlaySound(AudioController.Sounds.point);
        }
    }
}
