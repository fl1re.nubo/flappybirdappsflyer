using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public float obstacleSpeed;

    private void Update()
    {
        transform.position += ((Vector3.left * obstacleSpeed) * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Destroyer")
        {
            Destroy(gameObject);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out ObstacleSpawner obstacle))
        {
            EventManager.SpawnOnDesroy();
        }
    }
}
