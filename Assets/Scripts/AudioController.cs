using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public AudioSource audioSource;
    public AudioClip[] audioClips;
    public enum Sounds
    {
        hit,
        point,
        wing
    }
    public Sounds soundState;

    public void PlaySound(Sounds sound)
    {
        switch(sound)
        {
            case Sounds.hit:
                audioSource.PlayOneShot(audioClips[0]);
                break;
            case Sounds.point:
                audioSource.PlayOneShot(audioClips[1]);
                break;
            case Sounds.wing:
                audioSource.PlayOneShot(audioClips[2]);
                break;
        }
    }
}
