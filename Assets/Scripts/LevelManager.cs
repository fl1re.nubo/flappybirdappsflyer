using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    [SerializeField] private GameObject startButton;
    [SerializeField] private Text scoreText;
    [SerializeField] private Text bestScore;
    [SerializeField] private AudioSource audioSource;

    private void Awake()
    {
        audioSource.volume = PlayerPrefs.GetFloat("volume");
    }

    private void Start()
    {

        startButton.GetComponent<Button>().onClick.AddListener(()=>EventManager.OnRestart());
    }
    private void OnEnable()
    {
        EventManager.onGameOver += GameOver;
        EventManager.onRestart += RestartGame;
        EventManager.onScore += CountScore;
    }
    private void OnDisable()
    {
        EventManager.onGameOver -= GameOver;
        EventManager.onRestart -= RestartGame;
        EventManager.onScore -= CountScore;
    }
    private void OnDestroy()
    {
        EventManager.onGameOver -= GameOver;
        EventManager.onRestart -= RestartGame;
        EventManager.onScore -= CountScore;
    }

    public void GameOver()
    {
        Time.timeScale = 0;
        startButton.SetActive(true);
        scoreText.gameObject.SetActive(false);
        bestScore.text = $"Score: {scoreText.text}\nBest Score: {CalculateScore(int.Parse(scoreText.text))}";

    }

    public int CalculateScore(int score)
    {
        if(PlayerPrefs.HasKey("BestScore"))
        {
            if(PlayerPrefs.GetInt("BestScore") > score)
            {
                score = PlayerPrefs.GetInt("BestScore");
                return score;
            }
            else
            {
                PlayerPrefs.SetInt("BestScore", score);
                return score;
            }
        }
        else
        {
            PlayerPrefs.SetInt("BestScore", score);
            return score;
        }
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(1);
        Time.timeScale = 1;
    }
    public void CountScore()
    {
        scoreText.text = (int.Parse(scoreText.text) + 1).ToString();
    }
}
