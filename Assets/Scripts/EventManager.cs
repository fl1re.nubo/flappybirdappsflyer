using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    public delegate void OnObjectDestroy();
    public static event OnObjectDestroy onDestroy;
    public delegate void OnGameOver();
    public static event OnGameOver onGameOver;
    public delegate void OnGameRestart();
    public static event OnGameRestart onRestart;
    public delegate void OnScore();
    public static event OnScore onScore;

    public static void SpawnOnDesroy()
    {
        onDestroy?.Invoke();
    }
    public static void GameOver()
    {
        onGameOver?.Invoke();
    }
    public static void OnRestart()
    {
        onRestart?.Invoke();
    }
    public static void OnGameScore()
    {
        onScore?.Invoke();
    }
}
