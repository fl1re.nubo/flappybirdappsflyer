using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    [SerializeField] private Button startButton;
    [SerializeField] private Toggle[] toggleGroup;
    [SerializeField] private Slider volumeSlider;

    private void Awake()
    {
        InitSettings();
    }

    private void Start()
    {

    }

    private void InitSettings()
    {
        startButton.onClick.AddListener(() => SceneManager.LoadScene(2));
        int difficult = PlayerPrefs.GetInt("difficult");
        switch(difficult)
        {
            case 1:
                toggleGroup[0].isOn = true;
                break;
            case 2:
                toggleGroup[1].isOn = true;
                break;
            case 3:
                toggleGroup[2].isOn = true;
                break;
        }
        float volume = PlayerPrefs.GetFloat("volume");
        volumeSlider.value = volume;
    }
    public void SetDifficult(int num)
    {
        PlayerPrefs.SetInt("difficult", num);
        Debug.Log("difficult set");
    }
    public void SetVolume(Slider slider)
    {
        PlayerPrefs.SetFloat("volume", slider.value);
        Debug.Log($"volime is {slider.value}");
    }
}
