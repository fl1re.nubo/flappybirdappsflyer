using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour
{
    [SerializeField] private GameObject prefab;
    [SerializeField] private Transform placeToSpawn;
    [SerializeField] private int difficult;

    private void Awake()
    {
        difficult = PlayerPrefs.GetInt("difficult");
    }

    private void Start()
    {
        SpawnObject();
    }

    private void OnEnable()
    {
        EventManager.onDestroy += SpawnObject;
    }

    private void OnDisable()
    {
        EventManager.onDestroy -= SpawnObject;
    }
    private void SpawnObject()
    {
        GameObject go = Instantiate(prefab, placeToSpawn);
        go.transform.position = new Vector3(go.transform.position.x + Random.Range(0f,2f),Random.Range(-3f,1.9f),0);
        go.TryGetComponent<Obstacle>(out Obstacle obstacle);
        switch (difficult)
        {
            case 1:
                obstacle.obstacleSpeed = 2f;
                break;
            case 2:
                obstacle.obstacleSpeed = 3.5f;
                break;
            case 3:
                obstacle.obstacleSpeed = 5f;
                break;
        }
    }


}
